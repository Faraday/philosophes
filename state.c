/*
** state.c for philosophe in /home/faraday/projects/C/philosophes
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Thu Feb 19 14:38:20 2015 raphael elkaim
** Last update Wed Feb 25 11:21:40 2015 arthur
*/

#include <stdio.h>
#include <unistd.h>
#include "philo.h"
#include "list.h"

int	go_eat(t_philo *philo, int flag)
{
  if (!flag && pthread_mutex_trylock(&philo->is_available))
    return (0);
  if (pthread_mutex_trylock(&philo->next->is_available))
    return (0);
  printf("%sPhilosopher %d ate: he has %d left%s\n", GREEN, philo->nb, philo->rice - 1, END);
  sleep(3);
  philo->rice -= 1;
  pthread_mutex_unlock(&philo->is_available);
  pthread_mutex_unlock(&philo->next->is_available);
  return (1);
}

int	thinking(t_philo *philo)
{
  int	i;

  i = 0;
  if (pthread_mutex_trylock(&philo->is_available))
    return (0);
  printf("%sPhilosopher %d thinking%s\n", RED, philo->nb, END);
  while (i < 20)
    {
      if (go_eat(philo, 1))
	return (1);
      sleep(1);
      ++i;
    }
  pthread_mutex_unlock(&philo->is_available);
  return (0);
}

void	rest(t_philo *philo)
{
  while (1)
    {
      if (!philo->rice)
	break ;
      printf("%sPhilosopher %d is resting%s\n", BLUE, philo->nb, END);
      sleep(2);
      if (thinking(philo))
	continue ;
    }
}
