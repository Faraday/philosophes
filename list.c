/*
** list.c for list in /home/amstuta/rendu/philosophes
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Tue Feb 17 11:33:57 2015 arthur
** Last update Wed Feb 25 14:03:29 2015 raphael elkaim
*/

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include "list.h"

int		add_to_list_end(t_philo **list, char nb)
{
  t_philo	*new;
  t_philo	*tmp;

  tmp = *list;
  if ((new = malloc(sizeof(t_philo))) == NULL)
    return (1);
  new->nb = nb + 1;
  new->rice = 10;
  if (pthread_mutex_init(&new->is_available, NULL))
    return (2);
  if (*list == NULL)
    {
      new->prev = new;
      new->next = new;
      *list = new;
      return (0);
    }
  while (tmp->next != *list)
    tmp = tmp->next;
  tmp->next->prev = new;
  tmp->next = new;
  new->prev = tmp;
  new->next = *list;
  return (0);
}

void		dump(t_philo *list)
{
  t_philo	*tmp;

  tmp = list;
  if (list == NULL)
    return ;
  //  printf("Nb: %d, Stick: %d\n", tmp->nb, tmp->has_available_stick);
  tmp = tmp->next;
  /*  while (tmp != list && tmp != NULL)
    {
      printf("Nb: %d, Stick: %d\n", tmp->nb, tmp->has_available_stick);
      tmp = tmp->next;
      }*/
}
