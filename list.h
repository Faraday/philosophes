/*
** list.h for list in /home/amstuta/rendu/philosophes
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Tue Feb 17 13:24:48 2015 arthur
** Last update Wed Feb 25 14:03:05 2015 raphael elkaim
*/

#ifndef LIST_H_
# define LIST_H_

#include <pthread.h>

typedef struct		s_philo
{
  char			nb;
  pthread_mutex_t	is_available;
  int			rice;
  struct s_philo	*next;
  struct s_philo	*prev;
}			t_philo;

int			add_to_list_end(t_philo **, char);
void			dump(t_philo *);

#endif
