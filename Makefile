##
## Makefile for makefile in /home/amstuta/rendu/philosophes
##
## Made by arthur
## Login   <amstuta@epitech.net>
##
## Started on  Tue Feb 17 11:10:43 2015 arthur
## Last update Thu Feb 19 14:51:05 2015 raphael elkaim
##

CC	=	gcc

CFLAGS	=	-Wall -Wextra -Werror -lpthread

RM	=	rm -f

SRCS	=	main.c \
		list.c \
		dinner.c \
		state.c

NAME	=	philo

OBJS	=	$(SRCS:.c=.o)

all:	$(NAME)

$(NAME):$(OBJS)
	$(CC) $(OBJS) -o $(NAME) -lpthread

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re:	clean fclean all

.PHONY:	all clean fclean re
