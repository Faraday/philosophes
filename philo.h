/*
** philo.h for philosophes in /home/faraday/projects/C/philosophes
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Wed Feb 18 16:44:09 2015 raphael elkaim
** Last update Wed Feb 25 13:52:05 2015 raphael elkaim
*/

#ifndef PHILO_H_
# define PHILO_H_

#define	GREEN	"\x1B[32m"
#define	RED	"\x1B[31m"
#define BLUE	"\x1B[34m"
#define	END	"\x1B[37m"

#include "list.h"

void	*at_the_table(void *lel);
void	start_eating();
void	rest(t_philo *);
int	thinking(t_philo *);

#endif
