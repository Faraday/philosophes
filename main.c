/*
** main.c for main in /home/amstuta/rendu/philosophes
**
** Made by arthur
** Login   <amstuta@epitech.net>
**
** Started on  Tue Feb 17 11:19:08 2015 arthur
** Last update Wed Feb 25 14:14:37 2015 raphael elkaim
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "philo.h"
#include "list.h"

void		create_philosophers(t_philo **list)
{
  int		i;
  int		ret;

  i = 0;
  while (i < 7)
    {
      ret = add_to_list_end(list, i);
      if (ret == 1)
	{
	  printf("a philosopher's mother had an unexpected abortion");
	  printf(" in the past and as such he couldn't come and eat.\n");
	  exit(EXIT_FAILURE);
	}
      if (ret == 2)
	{
	  printf("a philospher broke his stick on the way and decided");
	  printf(" they would be having this meeting another day.\n");
	  exit(EXIT_FAILURE);
	}
      ++i;
    }
}

int		main()
{
  t_philo	*list;

  list = NULL;
  create_philosophers(&list);
  start_eating(list);
  return (0);
}
