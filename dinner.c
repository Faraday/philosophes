/*
** dinner.c for mon_premier_projet in /home/faraday/projects/C/philosophes
**
** Made by raphael elkaim
** Login   <elkaim_r@epitech.net>
**
** Started on  Wed Feb 18 16:30:01 2015 raphael elkaim
** Last update Wed Feb 25 14:28:56 2015 raphael elkaim
*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "philo.h"
#include "list.h"

void		*at_the_table(void *arg)
{
  t_philo	*philo = (t_philo *)arg;
  
  sleep(philo->nb);
  rest(philo);
  pthread_exit(0);
}

void		start_eating(t_philo *list)
{
  int		i;
  void		*ret;
  pthread_t	eaters[7];

  i = 0;
  while (i < 7 && list)
    {
      if (pthread_create(&(eaters[i]), NULL, at_the_table, list))
	{
	  printf("a philosopher tried to join ");
          printf("the table but fell on a rock and died\n");
	  exit(EXIT_FAILURE);
	}
      printf("philoshophe number %d joined the table!\n", list->nb);
      ++i;
      list = list->next;
    }
  i = 0;
  while (i < 7)
    {
      pthread_join(eaters[i], &ret);
      ++i;
    }
}
